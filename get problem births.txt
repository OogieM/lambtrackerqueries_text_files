--	Need updating for new database structure
SELECT 
	sheep_table.sheep_id
	, sheep_table.sheep_name
	, sheep_table.birth_date
	, sheep_sex_table.sex_abbrev
	, birth_type_table.birth_type
	, custom_evaluation_traits_table.custom_evaluation_item
	, sire_table.sheep_name AS sire_name
	, dam_table.sheep_name AS dam_name 
FROM sheep_table 
LEFT JOIN birth_type_table ON sheep_table.birth_type = birth_type_table.id_birthtypeid 
LEFT JOIN sheep_sex_table ON sheep_table.sex = sheep_sex_table.sex_sheepid 
LEFT JOIN sheep_table AS sire_table ON sheep_table.sire_id = sire_table.sheep_id
LEFT JOIN sheep_table AS dam_table ON sheep_table.dam_id = dam_table.sheep_id
INNER JOIN custom_evaluation_traits_table ON sheep_table.lambease = custom_evaluation_traits_table.id_custom_traitid
WHERE 
	(sheep_table.remove_date IS NULL OR sheep_table.remove_date IS '' )
	AND (sheep_table.sire_id IS NOT NULL OR sheep_table.dam_id IS NOT NULL) 
	AND (sheep_table.lambease = 10 OR sheep_table.lambease= 11 OR sheep_table.lambease= 12 OR sheep_table.lambease= 13)
	AND sheep_table.birth_date LIKE "2014%"
ORDER BY 
	sheep_table.birth_date ASC
	, dam_name

-- Find ewes who had problems with at least 1 lamb this year
-- This still works with new database structure
SELECT DISTINCT
  	dam_table.sheep_id
	, dam_table.sheep_name AS dam_name 
FROM sheep_table 
LEFT JOIN sheep_table AS dam_table ON sheep_table.dam_id = dam_table.sheep_id
INNER JOIN custom_evaluation_traits_table ON sheep_table.lambease = custom_evaluation_traits_table.id_custom_traitid
WHERE 
	(sheep_table.lambease= 10 OR sheep_table.lambease= 11 OR sheep_table.lambease= 12 OR sheep_table.lambease= 13)
	AND sheep_table.birth_date LIKE "2018%"
ORDER BY
	dam_name


